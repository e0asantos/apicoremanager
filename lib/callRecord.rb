#!/usr/bin/env ruby
# encoding: utf-8
#Encoding.default_external = Encoding::UTF_8
#Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = "utf-8"
Encoding.default_internal  = "utf-8"
require 'rubygems'
require 'uri'
require 'net/http'
require 'json'
require 'active_record'
require '/usr/src/apiManagerCore/db/model/sys_phrases.rb'

class CallRecord

  attr_accessor:json_result
  attr_accessor:text
  attr_accessor:db_message
  attr_accessor:continue_conversation
  attr_accessor:call_id

  def initialize(lang, userphone,callid=nil)
    if callid!=nil
          @call_id=callid
    end
    @text="NONE"
    @continue_conversation=false
    userlang=lang
    uid=UUIDTools::UUID.random_create.to_s()
    tmpfile='/tmp/'+uid
    #tmpfile='/tmp/4c0fd24e-95da-4980-ae22-2ccf916cd4e3'
    puts 'RECORD FILE '+tmpfile+' sln "0" "-1" BEEP "s=3"'
    puts "converting"
    systemResult=false
    contador=0
    while systemResult==false
      convertString='/usr/bin/flac -8 --totally-silent --channels=1 --endian=little --sign=signed --bps=16 --force-raw-format --sample-rate=16000 '+tmpfile+'.sln'
      contador=contador+1
      puts contador
      puts convertString
      systemResult=system(convertString)
      if contador>390
        systemResult=true
      end
    end
    
    puts systemResult
    puts "converted "
    puts "sending google...."
    result = %x[/usr/bin/perl /usr/src/apiManagerCore/lib/callRecordHelper.pl #{tmpfile}.flac #{userlang}]
    system("/usr/bin/flac -dc "+tmpfile+".flac | lame -b 320 - /usr/src/lineAccess/public/calls/"+uid+".mp3")
    puts "-----"
    puts result
    puts "-----"
    @json_result=JSON.parse(result)
    if @json_result["hypotheses"].length>0
      @text=@json_result["hypotheses"][0]["utterance"]
      negativeText=". "+@text+" ."
      nuevaFrase=Sys_Phrase.new
      nuevaFrase.phrase=@text
      nuevaFrase.phone=userphone
      nuevaFrase.soundfile=uid
      nuevaFrase.callid=@call_id
      nuevaFrase.save
      @db_message=nuevaFrase
      if @text.downcase.index(" no ")!=nil or @text.downcase.index(" not ")!=nil
        @continue_conversation=false
      else
        @continue_conversation=true
      end
    end
  end
end
