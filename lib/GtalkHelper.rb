require 'rubygems'
require 'xmpp4r/roster'
require 'xmpp4r_facebook'
require 'xmpp4r'
require 'active_record'
require 'yaml'
require '/usr/src/apiManagerCore/db/model/api_interface.rb'
require '/usr/src/apiManagerCore/db/model/api_token_user_interface.rb'
require '/usr/src/apiManagerCore/db/model/sys_user.rb'

class ChatHelper

	attr_accessor:last_message
	attr_accessor:chatTokens
	attr_accessor:usuarioAccount
	attr_accessor:chatfrom
	attr_accessor:chatto
	attr_accessor:chatclient
	attr_accessor:chatUserToken

	def initialize(myuserId,interfaceId)
		db_config = YAML::load( File.open("/usr/src/apiManagerCore/db/database.yml"))
		ActiveRecord::Base.establish_connection( db_config["lineAccess"])
		@chatTokens=Api_Interface.find_by_id(interfaceId)
		@usuarioAccount=Sys_User.find_by_id(myuserId)
		if @chatTokens.name=="google"
			@chatfrom=@usuarioAccount.email	
		elsif @chatTokens.name=="facebook"
			@chatfrom='-'+@usuarioAccount.user_id+'@chat.facebook.com'
		end
			
		
		puts "chatfrom:"+@chatfrom
		@chatUserToken=Api_Token_User_Interface.where("sys_user="+@usuarioAccount.id.to_s+' and api_interface='+interfaceId.to_s)[0]
		@chatUserToken.forceUpdateToken()
		puts @chatUserToken.token
		@chatclient=Jabber::Client.new Jabber::JID.new(@chatfrom)
		@chatclient.connect
		puts "preconnect gtalk helper"
		if @chatTokens.name=="google"
			@chatclient.auth_sasl(Jabber::SASL.new(@chatclient, 'X-OAUTH2'), @chatUserToken.token)
		elsif @chatTokens.name=="facebook"
			puts @chatTokens.token+"___"+@chatUserToken.token+"______"+@chatTokens.token_secret
			@chatclient.auth_sasl(Jabber::SASL::XFacebookPlatform.new(@chatclient, @chatTokens.token, @chatUserToken.token, @chatTokens.token_secret), nil)
		end
		puts "after connect gtalk helper"
	end

	def sendMessage(incomingMessage,userTo)
		@chatto=userTo
		puts "sendMessage gtalk mod"
		puts incomingMessage
		body = "uno dos tres cuatro cinco seis"
		message = Jabber::Message.new @chatto, incomingMessage
		@chatclient.send message
	end

end