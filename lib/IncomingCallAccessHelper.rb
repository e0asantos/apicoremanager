#!/usr/bin/env ruby
# encoding: utf-8
#Encoding.default_external = Encoding::UTF_8
#Encoding.default_internal = Encoding::UTF_8
require 'yaml'
require 'active_record'
require '/usr/src/apiManagerCore/db/model/sys_metric_access.rb'

class IncomingCallAccessHelper
	attr_accessor:incomingMetric

	def initialize(callerid,calleridname)
		db_config = YAML::load( File.open("/usr/src/apiManagerCore/db/database.yml"))
		ActiveRecord::Base.establish_connection( db_config["lineAccess"])
		@incomingMetric=Sys_Metric_Access.new
		@incomingMetric.phoneNumber=callerid
		@incomingMetric.voipAlias=calleridname
		@incomingMetric.sys_access_type=1
		@incomingMetric.start_date=Time.now.to_s(:db)
		@incomingMetric.save
	end

	def endCall(reason)
		@incomingMetric.end_date=Time.now.to_s(:db)
  		@incomingMetric.closeReason=reason
  		@incomingMetric.save
	end


end