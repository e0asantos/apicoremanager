require 'koala'
require '/usr/src/apiManagerCore/core/core.rb'

class FacebookClient
	attr_accessor:user_id
	attr_accessor:graph

def initialize(user_phone)  
    @user_id=get_user_id(user_phone)
    #get token (2 = facebook api)
    token = get_token(2, user_id)
    @graph = Koala::Facebook::API.new(token)
end

def post_wall(message)
	if @graph
		@graph.put_connections("me", "feed", :message => message)	
		# save post in DB
		save_note(2,@user_id,message)	
	end
end

end