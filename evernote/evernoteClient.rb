# autor :Héctor Acosta
# email :hacost@hotmail.com
# date  :2013-jun-29
# file  :evernoteClient.rb  
# description: evernote with ruby 

require 'evernote_oauth'
require '/usr/src/apiManagerCore/core/core.rb'
require '/usr/src/apiManagerCore/evernote/evernoteContent.rb'

class EvernoteClient
	attr_accessor:authToken
	attr_accessor:client
	attr_accessor:user_store
	attr_accessor:user_id
	attr_accessor:note_store
def initialize(user_phone)  
	@user_id=get_user_id(user_phone)
	puts "-----evernoteclient----"
	puts @user_id
    #get token (1 = evernote api)
    @authToken = get_token(1, @user_id)
    @client = EvernoteOAuth::Client.new(token: authToken)
    @note_store =  client.note_store
    @user_store = client.user_store 
end

def getNoteBooks()
	return note_store.listNotebooks(authToken)
end 

def getUser()
    return user_store.getUser
end

def createNote(body_note)
	if authToken
		# To create a new note, simply create a new Note object and fill in
		# attributes such as the note's title.
		note = Evernote::EDAM::Type::Note.new
		note.title = "note create by lineaccess"
		note.tagNames = ['line', 'access', 'lineaccess']
		# Now, add the new Resource to the note's list of resources
		note.resources = [ getResources() ]

		# The content of an Evernote note is represented using Evernote Markup Language
		# (ENML). The full ENML specification can be found in the Evernote API Overview
		# at http://dev.evernote.com/documentation/cloud/chapters/ENML.php
		note.content = getContent(note.title, body_note)

		# Finally, send the new note to Evernote using the createNote method
		# The new Note object that is returned will contain server-generated
		# attributes such as the new note's unique GUID.
		createdNote = note_store.createNote(authToken, note)

		# save note in DB
		save_note(1,@user_id,body_note)
	end	
end

end