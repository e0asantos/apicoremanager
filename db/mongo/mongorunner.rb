
require './mongoCore.rb'

#interfaces
@mc = MongoCore.new("api_interface")
@mc.create({"name"=>"Evernote", "removed" => false} 
	{"_id"=> "note", "name"=>"Create Note", "oral"=>true, "removed"=>false}
	{"_id"=> "findNote", "name"=>"Find Note", "oral"=>true, "removed"=>false})
# roles
@mc.setCollection("sys_rol")
@mc.create({"_id"=>"user" "name"=> "User", "removed" => false})
@mc.create({"_id"=>"admin" "name"=> "Admin", "removed" => false})
#type phones
@mc.setCollection("sys_type_phone")
@mc.create({"type"=>"movil", "description"=>"Cell Phone"})
@mc.create({"type"=>"landline", "description"=>"Landline Phone"})
#usuarios
@mc.setCollection("sys_user")
@mc.create(
	{"name"=> "Héctor", "last_name"=>"Acosta", "email"=>"hacost@hotmail.com", "age"=> 25, "user"=> "hacost", "password"=>"secreta", "sys_rol"=>"admin", "removed"=>false})

#@mc.deleteById("51c8e5d72f5d3027a0000001")


#puts "There are #{@coll.count} records. Here they are:"
# @mc.getAll().each { |doc| puts doc.inspect }

@mc.find({age: 25}).each { |doc| puts doc.inspect }
