# autor :Héctor Acosta
# email :hacost@hotmail.com
# date  :24-jun-2013
# file  :mongoCore.rb  
# description: mongoDB with ruby  

require './mongoConnection.rb'

class MongoCore
attr_accessor:collection
attr_accessor:db
attr_accessor:connection
  def initialize(collection)  
    @connection = MongoConnection.new()
    @db =@connection.getDB()
    @collection = @db[collection]
  end

  def setCollection(collection)
     @collection = @db[collection]
  end

  def create(json)
  	@collection.insert(json)
  end

  def update(id, jason)
  end

  def deleteById(value)
      @collection.remove({_id: BSON::ObjectId(value)}, 1) 
  end

  def deleteByName(value)
      @collection.remove({name: value}, 1) 
  end
  
  def find(value)
      @collection.find(value)
  end

  def getAll()
      @collection.find()
  end

  def getCollection()
    return @collection
  end

  private
  
end
