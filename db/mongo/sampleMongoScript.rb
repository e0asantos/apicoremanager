require 'rubygems'
require 'mongo'



@client = Mongo::Connection.new('localhost', 27017)
@db     = @client['sample-db']
@coll   = @db['test']

@coll.remove

 3.times do |i|
   @coll.insert({'a' => i+1})
 end
puts @client.database_names     # lists all database names
@client.database_info.each { |info| puts info.inspect }


puts "There are #{@coll.count} records. Here they are:"
@coll.find.each { |doc| puts doc.inspect }