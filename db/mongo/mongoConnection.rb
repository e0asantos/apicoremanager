# autor :Héctor Acosta
# email :hacost@hotmail.com
# date  :24-jun-2013
# file  :mongoConnection.rb  
# description: this class create one connection to mongoDB with ruby  

require 'rubygems'
require 'mongo'

class MongoConnection
attr_accessor:connection 
attr_accessor:db
  def initialize()  
    @connection = Mongo::MongoClient.new('localhost', 27017)
    #@db = @connection['lineAccess']
    @db = @connection['sample-db']
  end 

  def getDB()
  	return @db
  end

end

