# encoding: utf-8


# algo=Sys_Phrase.new
# algo.phrase="quiero escribir un mensaje en facebook hola cómo estás"
# algo.phone="8180829367"
# algo.interface_executed=4
# algo.save


# The speak method calls a text-to-speech interface in the supplied language.
# It does not translate the text. Format can be 'audio/mp3' or 'audio/wav'

require 'active_record'
require 'yaml'
require '/usr/src/apiManagerCore/db/model/api_interface.rb'
db_config = YAML::load( File.open("/usr/src/apiManagerCore/db/database.yml"))
ActiveRecord::Base.establish_connection( db_config["lineAccess"])