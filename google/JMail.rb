require 'gmail_xoauth'
require 'gmail'
require 'gmail-contacts'
require 'active_support'
require 'rest_client'
require '/usr/src/apiManagerCore/db/model/api_token_user_interface.rb'
require '/usr/pbx-ruby/jarvix/JarvixPerson.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Abstract_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Property.rb'
require '/usr/src/apiManagerCore/db/model/sys_user_phone.rb'
require '/usr/src/apiManagerCore/db/model/sys_user.rb'


class JMail

	attr_accessor:fromIdUser
	attr_accessor:toIdUser
	attr_accessor:JObject
	attr_accessor:currentToken
	attr_accessor:fromPhone

	attr_accessor:mailFrom
	attr_accessor:mailTo
    
    def _write(to,message)
        _send(to,message)    
    end
	
	def _send(to,message)
		#primero verificar que el usuario tenga mail
		vfrom=userHasMail(@fromIdUser,true)
		puts "vfrom PASSED:"+vfrom
		vto=userHasMail(to)
		puts "vto PASSED:"+vto
		if vfrom!="OK"
			return vfrom
		elsif vto!="OK"
			return vto
		end
		puts "PASSED"
		#las direcciones se verificaron ahora crear objeto de mail
		lineAccessUser=JarvixToLineAccessUser(@fromIdUser)
		#obtener el token del email
		#buscar token
		@currentToken=Api_Token_User_Interface.where(:api_interface=>6,:sys_user=>lineAccessUser.id).last
		@currentToken.forceUpdateToken()
		@JObject = Gmail.new(lineAccessUser.email, @currentToken.token)
		emailTo=getUserEmail(to)
		puts "eMail is from:"+lineAccessUser.email+" and goes to:"+emailTo
		@JObject.deliver do
		  to emailTo
		  subject "LineAccess"
		  text_part do
		    body message
		  end
		  # html_part do
		  #   content_type 'text/html; charset=UTF-8'
		  #   body "<p>Text of <em>html</em> message.</p>"
		  # end
		  ##add_file "/path/to/some_image.jpg"
		end
		return "OK"
	end
	def createBody
		
	end
	def userHasMail(userID,verifyGmail=false)
		realEmail=getUserEmail(userID)
		if realEmail.index("E:")
			return "the user "+realEmail.split(":")[1]+" doesnt have a mail to send"
		end
		#verificar email si es gmail
		if (realEmail.index("gmail.com")!=nil and verifyGmail==true) or verifyGmail==false
			#si es gmail, ahora verificar la validez del mail
			if validate_email_domain(realEmail)==true
				return "OK"
			else
				return "the user "+person.ontosData.my_name+" doesnt have a valid mail to send"
			end
		else
			return "only gmail accounts can send mails"
		end
	end
	def getUserEmail(userID)
		#buscar ususario en jarvix y buscar su telefono
		#para poder buscarlo en lineaccess
		person=JarvixPerson.new(userID)
		emailWord=Jarvix_Abstract_Object.find_by_object_name("email")
		#buscar si lo tiene asignado
		personEmail=person.getObjectProperty("email")#Jarvix_Custom_Object.where(:id_abstract_object=>emailWord.id,:id_owner=>person.ontosData.id)
		if personEmail!=nil
			puts "personEmail:"+personEmail.inspect
			return personEmail
		else
			return "E:"+person.ontosData.my_name
		end
		
		# if personEmail.length<1
		# 	return "E:"+person.ontosData.my_name
		# end
		# #parece que tiene, ahora determinar si el email es correcto
		# realEmail=Jarvix_Custom_Property.where(:property_name=>"extra",:id_user=>person.ontosData.id,:id_custom_object=>personEmail[0].id).last
		# if realEmail.nil?
		# 	return "E:"+person.ontosData.my_name
		# end
		# return realEmail.property_value
	end
	def validate_email_domain(email)
      domain = email.match(/\@(.+)/)
      if domain!=nil
      	domain=domain[1]
      else
      	return false
      end
      Resolv::DNS.open do |dns|
          @mx = dns.getresources(domain, Resolv::DNS::Resource::IN::MX)
      end
      @mx.size > 0 ? true : false
	end
	def JarvixToLineAccessUser(userID)
		person=JarvixPerson.new(userID)
		#buscar telefono
		personPhone=Jarvix_Custom_Object.where(:id_abstract_object=>1,:id_owner=>person.ontosData.id).last
		realPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>person.ontosData.id,:id_custom_object=>personPhone).last
		#con el telefono podemos buscar el usuario
		lineAccessPhone=Sys_User_Phone.where(:number=>realPhone.property_value,:sys_phone_type=>1).last
		lineAccessUser=Sys_User.find_by_id(lineAccessPhone.sys_user)

	end

end