require 'rubygems'
require 'xmpp4r/roster'
require 'xmpp4r_facebook'
require 'xmpp4r'
require 'active_record'
require 'yaml'
require '/usr/src/apiManagerCore/db/model/api_interface.rb'
require '/usr/src/apiManagerCore/db/model/api_token_user_interface.rb'
require '/usr/src/apiManagerCore/db/model/sys_user.rb'

require '/usr/pbx-ruby/app/models/Jarvix_Abstract_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Property.rb'
require '/usr/src/apiManagerCore/db/model/sys_user_phone.rb'

class ChatHelper

	attr_accessor:last_message
	attr_accessor:chatTokens
	attr_accessor:usuarioAccount
	attr_accessor:chatfrom
	attr_accessor:chatto
	attr_accessor:chatclient
	attr_accessor:chatUserToken

	@@isActiveChat=Hash.new

	def initialize(myuserId,interfaceId)
		puts "CHAT is ready"
	end

	def initialize
		puts "CHAT is ready zero params"
	end

	def self.isActiveChat
		#static method for verify activechats
		return @@isActiveChat
	end

	def start(jarvixIdFrom,jarvixIdTo,interfaceId)
		
		# db_config = YAML::load( File.open("/usr/src/apiManagerCore/db/database.yml"))
		# ActiveRecord::Base.establish_connection( db_config["lineAccess"])
		@chatTokens=Api_Interface.find_by_id(interfaceId)
		@usuarioAccount=JarvixToLineAccessUser(jarvixIdFrom)
		chat_to=JarvixToLineAccessUser(jarvixIdTo)
		if @chatTokens.name=="google"
			@chatfrom=@usuarioAccount.email
			@chatto=chat_to.email
		elsif @chatTokens.name=="facebook"
			@chatfrom='-'+@usuarioAccount.user_id+'@chat.facebook.com'
			@chatto='-'+chat_to.user_id+'@chat.facebook.com'
		end
			
		
		puts "chatfrom:"+@chatfrom
		@chatUserToken=Api_Token_User_Interface.where("sys_user="+@usuarioAccount.id.to_s+' and api_interface='+interfaceId.to_s)[0]
		@chatUserToken.forceUpdateToken()
		puts @chatUserToken.token
		@chatclient=Jabber::Client.new Jabber::JID.new(@chatfrom)
		@chatclient.connect
		@@isActiveChat["from_"+@usuarioAccount.id.to_s]=@chatclient
		puts "preconnect gtalk helper"
		if @chatTokens.name=="google"
			@chatclient.auth_sasl(Jabber::SASL.new(@chatclient, 'X-OAUTH2'), @chatUserToken.token)
		elsif @chatTokens.name=="facebook"
			puts @chatTokens.token+"___"+@chatUserToken.token+"______"+@chatTokens.token_secret
			@chatclient.auth_sasl(Jabber::SASL::XFacebookPlatform.new(@chatclient, @chatTokens.token, @chatUserToken.token, @chatTokens.token_secret), nil)
		end
		puts "after connect gtalk helper"
	end

	def sendMessage(incomingMessage)
		puts "sendMessage gtalk mod"
		puts incomingMessage
		body = "uno dos tres cuatro cinco seis"
		message = Jabber::Message.new @chatto, incomingMessage
		@chatclient.send message
	end

	def JarvixToLineAccessUser(userID)
		person=JarvixPerson.new(userID)
		#buscar telefono
		personPhone=Jarvix_Custom_Object.where(:id_abstract_object=>1,:id_owner=>person.ontosData.id).last
		realPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>person.ontosData.id,:id_custom_object=>personPhone).last
		#con el telefono podemos buscar el usuario
		lineAccessPhone=Sys_User_Phone.where(:number=>realPhone.property_value,:sys_phone_type=>1).last
		lineAccessUser=Sys_User.find_by_id(lineAccessPhone.sys_user)
	end

end