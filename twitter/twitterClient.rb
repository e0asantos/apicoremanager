require 'twitter'
require '/usr/src/apiManagerCore/core/core.rb'
require '/usr/src/apiManagerCore/twitter/clientConfig.rb'

class TwitterClient
	attr_accessor:user_id

def initialize(user_phone)  
    @user_id=get_user_id(user_phone)
    #get token and token secret (3 = twitter api)
    token = get_token(3, user_id)
    tokenSecret = get_token_secret(3, user_id)
    client_configure(token, tokenSecret)
end

def create_tweet(tweet)
	if Twitter
		Twitter.update(tweet)
		# save tweet in DB
		save_note(3,@user_id,tweet)		
	end	
end

private

def client_configure (token, tokenSecret)
	Twitter.configure do |config|
 	   config.consumer_key = TWITTER_CONSUMER_KEY
 	   config.consumer_secret = TWITTER_CONSUMER_SECRET
  	   config.oauth_token = token  
       config.oauth_token_secret = tokenSecret
	end
end
end
