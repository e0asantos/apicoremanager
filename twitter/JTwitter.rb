require 'gmail_xoauth'
require 'gmail'
require 'gmail-contacts'
require 'active_support'
require 'rest_client'
require '/usr/src/apiManagerCore/db/model/api_token_user_interface.rb'
require '/usr/pbx-ruby/jarvix/JarvixPerson.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Abstract_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Property.rb'
require '/usr/src/apiManagerCore/db/model/sys_user_phone.rb'
require '/usr/src/apiManagerCore/db/model/sys_user.rb'
require '/usr/src/apiManagerCore/twitter/twitterClient.rb'

class JTwitter

	attr_accessor:fromIdUser
	attr_accessor:toIdUser
	attr_accessor:JObject
	attr_accessor:currentToken
	attr_accessor:fromPhone

	def _publish(msg)
		return finallySend(msg)
	end

	def _create(msg)
		return finallySend(msg)
	end
	def _say(msg)
		return finallySend(msg)
	end
	def _write(msg)
		return finallySend(msg)
	end
	def _post(msg)
		return finallySend(msg)
	end
	def _dictate(msg)
		return finallySend(msg)
	end
	def finallySend(msg)
		if @fromPhone==nil
			@fromPhone=userPhone(@fromIdUser).number
		end
		@JObject=TwitterClient.new(@fromPhone)
		@JObject.create_tweet(msg)
		return "OK"
	end

	def jarvixToLineAccessUser(userID)
		#con el telefono podemos buscar el usuario.
		lineAccessPhone=userPhone(userID)
		lineAccessUser=Sys_User.find_by_id(lineAccessPhone.sys_user)
	end

	def userPhone(userID)
		person=JarvixPerson.new(userID)
		#buscar telefono
		personPhone=Jarvix_Custom_Object.where(:id_abstract_object=>1,:id_owner=>person.ontosData.id).last
		realPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>person.ontosData.id,:id_custom_object=>personPhone).last
		#con el telefono podemos buscar el usuario
		lineAccessPhone=Sys_User_Phone.where(:number=>realPhone.property_value,:sys_phone_type=>1).last
	end
end